# recipe-app-api-proxy 

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

 * `LISTEN_PORT` - Default Port(`8000`)
 * `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
 * `APP_PORT` - Port of the app to forward requests to (default: `9000`)

